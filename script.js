// console.log("Hello World")

// 3. Create a fetch request using the GET method that will retrieve all the to do list items from JSON Placeholder API.

fetch("https://jsonplaceholder.typicode.com/todos")
.then((response) => response.json())
.then((json) => console.log(json))

// 4. Using the data retrieved, create an array using the map method to return just the title of every item and print the result in the console.

fetch("https://jsonplaceholder.typicode.com/todos")
.then((response) => response.json())
.then((json) => {
    let list = [json.map(todos => todos.title)]
    console.log(list)
})

// 5. Create a fetch request using the GET method that will retrieve a single to do list item from JSON Placeholder API.

fetch("https://jsonplaceholder.typicode.com/todos/1")
.then((response) => response.json())
.then((json) => console.log(json))

// 6. Using the data retrieved, print a message in the console that will provide the title and status of the to do list item.

fetch("https://jsonplaceholder.typicode.com/todos/1")
.then((response) => response.json())
.then((json) => console.log(`The item "${json.title}" on the list has a status of ${json.completed}`))

// 7. Create a fetch request using the POST method that will create a to do list item using the JSON Placeholder API.

fetch("https://jsonplaceholder.typicode.com/todos", {
    method: "POST",
    headers:{
        "Content-Type":"application/json"
    },
    body: JSON.stringify({
        "completed":false,
        "title":"Created To Do List Item",
        "userId": 1
    })
})
.then((response) => response.json())
.then((json) => console.log(json))

// 8. Create a fetch request using the PUT method that will update a to do list item using the JSON Placeholder API.

fetch("https://jsonplaceholder.typicode.com/todos/1", {
    method: "PUT",
    headers:{
        "Content-Type":"application/json"
    },
    body: JSON.stringify({
        "dateCompleted":"Pending",
        "description":"To update the my to do list with a different data structure",
        "status":"Pending",
        "title":"Updated To Do List Item",
        "userId": 1
    })
})
.then((response) => response.json())
.then((json) => console.log(json))

/* Update a to do list item by changing the data structure to contain the following properties:
- Title
- Description
- Status
- Date Completed
- User ID
*/

fetch("https://jsonplaceholder.typicode.com/todos/1", {
    method: "PUT",
    headers:{
        "Content-Type":"application/json"
    },
    body: JSON.stringify({
        "completed": false,
        "status":"Complete",
        "title":"delectus aut autem",
        "userId": 1
    })
})
.then((response) => response.json())
.then((json) => console.log(json))

// Create a fetch request using the PATCH method that will update a to do list item using the JSON Placeholder API.

fetch("https://jsonplaceholder.typicode.com/todos/1", {
    method:"PATCH",
    headers: {
        "Content-Type":"application/json"
    },
    body: JSON.stringify({
        "completed":true,
        "dateCompleted":"07/09/21"
    })
})
.then((response) => response.json())
.then((json) => console.log(json))

// Create a fetch request using the DELETE method that will delete an item using the JSON Placeholder API.
fetch("https://jsonplaceholder.typicode.com/todos/1", {
    method: "DELETE"
})